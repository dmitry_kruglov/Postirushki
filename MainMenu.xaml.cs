﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp8
{
    /// <summary>
    /// Логика взаимодействия для MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        List<Class1> Washes = new List<Class1>();
        public MainMenu( Class1 newWash=null)
        {
            InitializeComponent();
            
            if(newWash != null)
            {
                Washes.Add(newWash);
            }
           
            Washes.Add(new Class1 { Num = "1", num_wash="машина №2", poroshok="Atial", sushka= "Да", people="Фамилия И.О." });
            Washes.Add(new Class1 { Num = "2", num_wash = "машина №1", poroshok = "Persil", sushka = "Нет", people = "Фамилия И.О." });
            Washes.Add(new Class1 { Num = "3", num_wash = "машина №5", poroshok = "Tide", sushka = "Да", people = "Фамилия И.О." });
        
            table1.ItemsSource = Washes;
            
        }

        private void AddWash_Click(object sender, RoutedEventArgs e)
        {
            Window AddWash = new AddWash(Washes.Count);
            AddWash.Show();
            Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Отчёт сохранён");
        }

        private void table1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Window StatusWash = new StatusWash();
            StatusWash.Show();
            Close();
        }
    }
}
