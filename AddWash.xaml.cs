﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp8
{
    /// <summary>
    /// Логика взаимодействия для AddWash.xaml
    /// </summary>
    public partial class AddWash : Window
    {
        int WashIndex;
        public AddWash(int LastIndex)
        {
            InitializeComponent();
            WashIndex = LastIndex;
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Стирка добавлена");
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ComboBoxItem WashMachine = (ComboBoxItem)cbxMachines.SelectedItem;
            ComboBoxItem Porosok = (ComboBoxItem)WashPorosok.SelectedItem; 

            string Drying = (bool)cbxDrying.IsChecked ? "Да" : "Нет";
            Class1 newWash = new Class1 { Num = (WashIndex+1).ToString(),num_wash = (string)WashMachine.Content, poroshok=(string)Porosok.Content, sushka = Drying, people = "Фамилия И.О."  };
            MessageBox.Show("Чек распечатан");
            Window MainMenu = new MainMenu(newWash);
            MainMenu.Show();
            Close();
        }
    }
}
